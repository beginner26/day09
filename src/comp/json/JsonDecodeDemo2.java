package comp.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

public class JsonDecodeDemo2 {

    public static void main(String[] args) throws IOException, ParseException {

        /*
        {
            "name":"John",
            "age":30,
            "cars": ["Ford","BMW","Fiat"]
        }
        */

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader("{\n" +
                "            \"name\":\"John\",\n" +
                "            \"age\":30,\n" +
                "            \"cars\": [\"Ford\",\"BMW\",\"Fiat\"]\n" +
                "        }");

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        String name = (String) jsonObject.get("name");
        System.out.println("Name = " + name);

        long age = (Long) jsonObject.get("age");
        System.out.println("Age = " + age);

        JSONArray jsonArray = (JSONArray) jsonObject.get("cars");

        ArrayList<String> arCars = new ArrayList<>();
        arCars = (ArrayList<String>) jsonArray;

        for (String el:arCars) {
            System.out.println(el);
        }
    }

}
