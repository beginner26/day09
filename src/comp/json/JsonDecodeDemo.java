package comp.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class JsonDecodeDemo {

    public static void main(String[] args) throws IOException, ParseException {

        /*
        {
         "name":"John",
         "age":30,
         "cars": [
                 { "name":"Ford", "models":[ "Fiesta", "Focus", "Mustang" ] },
                 { "name":"BMW", "models":[ "320", "X3", "X5" ] },
                 { "name":"Fiat", "models":[ "500", "Panda" ] }
                 ]
         }
        */

        JSONParser parser = new JSONParser();
        Reader reader = new StringReader("{\n" +
                " \"name\":\"John\",\n" +
                " \"age\":30,\n" +
                " \"cars\": [\n" +
                " { \"name\":\"Ford\", \"models\":[ \"Fiesta\", \"Focus\", \"Mustang\" ] },\n" +
                " { \"name\":\"BMW\", \"models\":[ \"320\", \"X3\", \"X5\" ] },\n" +
                " { \"name\":\"Fiat\", \"models\":[ \"500\", \"Panda\" ] }\n" +
                " ]\n" +
                " }");

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        String name = (String) jsonObject.get("name");
        System.out.println("Name: " + name);

        long age = (Long) jsonObject.get("age");
        System.out.println("Age: " + age);
//        JSONObject jsonObjCars = (JSONObject)jsonObject.get("cars");

        JSONArray jsonArray = (JSONArray) jsonObject.get("cars");

        System.out.println("Cars: [");
//        System.out.println(jsonArray);

        for(int i=0; i < jsonArray.size(); i++) {
            JSONObject cars = (JSONObject) jsonArray.get(i);
            String namee = (String) cars.get("name");

            // Array dari models
            JSONArray models = (JSONArray) cars.get("models");

            System.out.println("{\tname: "+namee);
            for (Object tipe: models) {
                System.out.println("\ttipe: "+tipe);
            }
            System.out.println("},");

        }



    }

}
