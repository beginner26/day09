package comp.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
class JsonEncodeDemo {
    public static void main(String[] args) {

        JSONArray fiat = new JSONArray();
        fiat.add("500");
        fiat.add("Panda");

        JSONArray bmw = new JSONArray();
        bmw.add("320");
        bmw.add("X3");
        bmw.add("X5");

        JSONArray ford = new JSONArray();
        ford.add("Fiesta");
        ford.add("Focus");
        ford.add("Mustang");


        JSONObject car1 = new JSONObject();
        car1.put("name", "Ford");
        car1.put("models", ford);

        JSONObject car2 = new JSONObject();
        car2.put("name", "BMW");
        car2.put("models", bmw);

        JSONObject car3= new JSONObject();
        car3.put("name", "Fiat");
        car3.put("models", fiat);

        JSONArray carLists = new JSONArray();
        carLists.add(car1);
        carLists.add(car2);
        carLists.add(car3);

        JSONObject data = new JSONObject();
        data.put("name", "john");
        data.put("age", 30);
        data.put("cars", carLists);

        System.out.println(data);

    }
}
