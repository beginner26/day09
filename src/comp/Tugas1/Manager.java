package comp.Tugas1;

import java.util.ArrayList;

public class Manager extends Worker {
    int tunjTransport;
    int tunjEntertaint;

    ArrayList<String> telepon = new ArrayList<>();

    public int getTunjTransport() {
        return tunjTransport;
    }

    public void setTunjTransport(int tunjTransport) {
        this.tunjTransport = tunjTransport;
    }

    public int getTunjEntertaint() {
        return tunjEntertaint;
    }

    public void setTunjEntertaint(int tunjEntertaint) {
        this.tunjEntertaint = tunjEntertaint;
    }

    public ArrayList<String> getTelepon() {
        return telepon;
    }

    public void setTelepon(ArrayList<String> telepon) {
        this.telepon = telepon;
    }




}
