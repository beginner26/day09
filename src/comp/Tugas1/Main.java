package comp.Tugas1;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        ArrayList<Manager> arM = new ArrayList<>();
        ArrayList<Staff> arS = new ArrayList<>();

//        Manager m = new Manager();
//        Staff s = new Staff();
//
//
//        Manager newManager1 = new Manager();
//        ArrayList<String> telepon1 = new ArrayList<>();
//        telepon1.add("0217756390");
//        telepon1.add("081290006155");
//
//        newManager1.setId(1);
//        newManager1.setNama("Udin");
//        newManager1.setTunjPulsa(500000);
//        newManager1.setGapok(4000000);
//        newManager1.setAbsensi(22);
//        newManager1.setTunjEntertaint(200000);
//        newManager1.setTunjTransport(300000);
//        newManager1.setTelepon(telepon1);
//        arM.add(newManager1);
//
//        Manager newManager2 = new Manager();
//        ArrayList<String> telepon2 = new ArrayList<>();
//        telepon2.add("0217756390");
//        telepon2.add("081290006155");
//
//        newManager2.setId(2);
//        newManager2.setNama("Mamat");
//        newManager2.setTunjPulsa(500000);
//        newManager2.setGapok(4000000);
//        newManager2.setAbsensi(22);
//        newManager2.setTunjEntertaint(200000);
//        newManager2.setTunjTransport(300000);
//        newManager2.setTelepon(telepon2);
//        arM.add(newManager2);
//
//
//
//         /**** 2. Create JSON format and write to manager ****/
//
//        JSONArray listManager = new JSONArray();
//
//        Iterator itr = arM.iterator();
//        while (itr.hasNext()) {
//            JSONObject dataManager = new JSONObject();
//            Manager mngr = (Manager) itr.next();
//
//           dataManager.put("id", mngr.getId());
//           dataManager.put("nama", mngr.getNama());
//           dataManager.put("tunjPulsa", mngr.getTunjPulsa());
//           dataManager.put("gapok", mngr.getGapok());
//           dataManager.put("absensi", mngr.getAbsensi());
//           dataManager.put("tunjTransport", mngr.getTunjTransport());
//           dataManager.put("tunjEntertaint", mngr.getTunjEntertaint());
//           dataManager.put("telepon", mngr.getTelepon());
//
//           listManager.add(dataManager);
//        }
//
//        JSONObject objManager = new JSONObject();
//        objManager.put("manager", listManager);
//
//        System.out.println(objManager);
//
//
//        // Write to File
//        FileOutputStream fout = new FileOutputStream("C:\\tmp\\manager.txt");
//        byte b[] = String.valueOf(objManager).getBytes();//converting string into byte array
//        fout.write(b);
//        fout.close();
//        System.out.println("success...");
//
//
//        /**** 3. Read JSON format from a file, input filename ****/
//        // Read from File
//        String manager = "";
//        FileReader fr = new FileReader("C:\\tmp\\manager.txt");
//        BufferedReader br = new BufferedReader(fr);
//        int i;
//        while ((i = br.read()) != -1) {
//            manager = manager + (char) i;
//        }
//        System.out.println("Success load data");
//        br.close();
//        fr.close();
//
//        JSONParser parser = new JSONParser();
//        Reader reader = new StringReader(manager);
//
//        Object jsonObj = parser.parse(reader);
//        JSONObject jsonObject = (JSONObject) jsonObj;
//
//        JSONArray managerJson = (JSONArray) jsonObject.get("manager");
////        System.out.println(managerJson);
//
//        for(int ic=0; ic < managerJson.size(); ic++) {
//            JSONObject dataManager = (JSONObject) managerJson.get(ic);
//
//            System.out.println("IDKaryawan: " +dataManager.get("id"));
//            System.out.println("Nama: " +dataManager.get("nama"));
//            System.out.println("Tunjangan Pulsa: " +dataManager.get("tunjPulsa"));
//            System.out.println("Gaji Pokok: " +dataManager.get("gapok"));
//            System.out.println("Absensi Hari: " +dataManager.get("absensi"));
//            System.out.println("Tunjangan Transport: " +dataManager.get("tunjTransport"));
//            System.out.println("Tunjangan Entertaint: " +dataManager.get("tunjEntertaint"));
//
//            JSONArray teleponn = (JSONArray) dataManager.get("telepon");
//            for (Object telp: teleponn) {
//                System.out.println("Telepon: " + telp);
//            }
//        }


        Scanner input = new Scanner(System.in);

        int userInput = 0;

        while(userInput != 99) {
            System.out.println("Menu");
            System.out.println("1. Buat Worker");
            System.out.println("2. Create JSON Format and Write to File");
            System.out.println("3. Read JSON Format from a File, input Filename(Show on Screen)");
            System.out.println("99. EXIT");
            System.out.print("Pilih Menu: ");
            userInput = input.nextInt();

            switch (userInput) {
                case 1:
                    System.out.println("a. Buat Staff");
                    System.out.println("b. Buat Manager");
                    System.out.print("Pilih: ");
                    String pilih = input.next();

                    if(pilih.equalsIgnoreCase("a")) {
                        System.out.println("Membuat data Staff");

                        Staff newStaff = new Staff();
                        ArrayList<String> email = new ArrayList<>();

                        System.out.print("Masukan ID: ");
                        int id = input.nextInt();
                        System.out.print("Masukan Nama: ");
                        String nama = input.next();
                        System.out.print("Masukan Tunjangan Pulsa: ");
                        int tunjPulsa = input.nextInt();
                        System.out.print("Masukan Gaji Pokok: ");
                        int gapok= input.nextInt();
                        System.out.print("Masukan Absensi: ");
                        int absensi = input.nextInt();

                        System.out.print("Masukan Tunjangan Makan: ");
                        int tunjMakan = input.nextInt();

                        System.out.print("Masukan Email Kantor: ");
                        String emailKantor = input.next();
                        System.out.print("Masukan Email Pribadi: ");
                        String emailPribadi = input.next();
                        email.add(emailKantor);
                        email.add(emailPribadi);

                        newStaff.setId(id);
                        newStaff.setNama(nama);
                        newStaff.setTunjPulsa(tunjPulsa);
                        newStaff.setGapok(gapok);
                        newStaff.setAbsensi(absensi);
                        newStaff.setTunjMakan(tunjMakan);
                        newStaff.setEmail(email);

                        arS.add(newStaff);
                        System.out.println("Berhasil menambahkan data Manager");

                    } else if (pilih.equalsIgnoreCase("b")) {
                        System.out.println("Membuat data Manager");

                        Manager newManager = new Manager();
                        ArrayList<String> telepon = new ArrayList<>();

                        System.out.print("Masukan ID: ");
                        int id = input.nextInt();
                        System.out.print("Masukan Nama: ");
                        String nama = input.next();
                        System.out.print("Masukan Tunjangan Pulsa: ");
                        int tunjPulsa = input.nextInt();
                        System.out.print("Masukan Gaji Pokok: ");
                        int gapok= input.nextInt();
                        System.out.print("Masukan Absensi: ");
                        int absensi = input.nextInt();
                        System.out.print("Masukan Tunjangan Entertaint: ");
                        int tunjEntertaint = input.nextInt();
                        System.out.print("Masukan Tunjangan Transport: ");
                        int tunjTransport = input.nextInt();
                        System.out.print("Masukan Telepon HP: ");
                        String tlpHp = input.next();
                        System.out.print("Masukan Telepon Rumah: ");
                        String tlpRumah = input.next();
                        telepon.add(tlpHp);
                        telepon.add(tlpRumah);

                        newManager.setId(id);
                        newManager.setNama(nama);
                        newManager.setTunjPulsa(tunjPulsa);
                        newManager.setGapok(gapok);
                        newManager.setAbsensi(absensi);
                        newManager.setTunjEntertaint(tunjEntertaint);
                        newManager.setTunjTransport(tunjTransport);
                        newManager.setTelepon(telepon);

                        arM.add(newManager);
                        System.out.println("Berhasil menambahkan data Manager");
                    }else{
                        System.out.println("Salah memasukan pilihan");
                    }
                    break;


                case 2:
                    System.out.println("Membuat JSON file");
                    System.out.println("a. Export Staff");
                    System.out.println("b. Export Manager");
                    System.out.print("Pilih Menu: ");

                    String pilihMenu2 = input.next();
                    if(pilihMenu2.equalsIgnoreCase("a")){
                        System.out.println("Export Staff");

                        JSONArray listStaff = new JSONArray();

                        Iterator itr = arS.iterator();
                        while (itr.hasNext()) {
                            JSONObject dataStaff = new JSONObject();
                            Staff st = (Staff) itr.next();

                            dataStaff.put("id", st.getId());
                            dataStaff.put("nama", st.getNama());
                            dataStaff.put("tunjPulsa", st.getTunjPulsa());
                            dataStaff.put("gapok", st.getGapok());
                            dataStaff.put("absensi", st.getAbsensi());
                            dataStaff.put("tunjMakan", st.getTunjMakan());
                            dataStaff.put("email", st.getEmail());

                            listStaff.add(dataStaff);
                        }

                        JSONObject objStaff = new JSONObject();
                        objStaff.put("staff", listStaff);

                        // Write to File
                        FileOutputStream fout = new FileOutputStream("C:\\tmp\\staff.txt");
                        byte b[] = String.valueOf(objStaff).getBytes();//converting string into byte array
                        fout.write(b);
                        fout.close();
                        System.out.println("success...");

                    } else if (pilihMenu2.equalsIgnoreCase("b")) {
                        System.out.println("Export Manager");
                        JSONArray listManager = new JSONArray();

                        Iterator itr = arM.iterator();
                        while (itr.hasNext()) {
                            JSONObject dataManager = new JSONObject();
                            Manager mngr = (Manager) itr.next();

                            dataManager.put("id", mngr.getId());
                            dataManager.put("nama", mngr.getNama());
                            dataManager.put("tunjPulsa", mngr.getTunjPulsa());
                            dataManager.put("gapok", mngr.getGapok());
                            dataManager.put("absensi", mngr.getAbsensi());
                            dataManager.put("tunjTransport", mngr.getTunjTransport());
                            dataManager.put("tunjEntertaint", mngr.getTunjEntertaint());
                            dataManager.put("telepon", mngr.getTelepon());

                            listManager.add(dataManager);
                        }

                        JSONObject objManager = new JSONObject();
                        objManager.put("manager", listManager);

                        // Write to File
                        FileOutputStream fout = new FileOutputStream("C:\\tmp\\manager.txt");
                        byte b[] = String.valueOf(objManager).getBytes();//converting string into byte array
                        fout.write(b);
                        fout.close();
                        System.out.println("success...");


                    }else{
                        System.out.println("Salah Pilih Menu");
                    }
                    break;


                case 3:
                    System.out.println("Read JSON file");
                    System.out.println("a. Read Staff");
                    System.out.println("b. Read Manager");
                    System.out.print("Pilih Menu: ");

                    String pilihMenu3 = input.next();

                    if(pilihMenu3.equalsIgnoreCase("a")){
                        System.out.println("Read data Staff");

                        String staff = "";
                        FileReader fr = new FileReader("C:\\tmp\\staff.txt");
                        BufferedReader br = new BufferedReader(fr);
                        int i;
                        while ((i = br.read()) != -1) {
                            staff = staff + (char) i;
                        }
                        System.out.println("Success load data");
                        br.close();
                        fr.close();

                        JSONParser parser = new JSONParser();
                        Reader reader = new StringReader(staff);

                        Object jsonObj = parser.parse(reader);
                        JSONObject jsonObject = (JSONObject) jsonObj;

                        JSONArray managerJson = (JSONArray) jsonObject.get("staff");
                //        System.out.println(managerJson);

                        for(int ic=0; ic < managerJson.size(); ic++) {
                            JSONObject dataStaff = (JSONObject) managerJson.get(ic);

                            System.out.println("IDKaryawan: " +dataStaff.get("id"));
                            System.out.println("Nama: " +dataStaff.get("nama"));
                            System.out.println("Tunjangan Pulsa: " +dataStaff.get("tunjPulsa"));
                            System.out.println("Gaji Pokok: " +dataStaff.get("gapok"));
                            System.out.println("Absensi Hari: " +dataStaff.get("absensi"));
                            System.out.println("Tunjangan Makan: " +dataStaff.get("tunjMakan"));

                            JSONArray email = (JSONArray) dataStaff.get("email");
                            for (Object eml: email) {
                                System.out.println("Email: " + eml);
                            }
                        }

                    } else if (pilihMenu3.equalsIgnoreCase("b")) {
                        System.out.println("Read data Manager");

                        String manager = "";
                        FileReader fr = new FileReader("C:\\tmp\\manager.txt");
                        BufferedReader br = new BufferedReader(fr);
                        int i;
                        while ((i = br.read()) != -1) {
                            manager = manager + (char) i;
                        }
                        System.out.println("Success load data");
                        br.close();
                        fr.close();

                        JSONParser parser = new JSONParser();
                        Reader reader = new StringReader(manager);

                        Object jsonObj = parser.parse(reader);
                        JSONObject jsonObject = (JSONObject) jsonObj;

                        JSONArray managerJson = (JSONArray) jsonObject.get("manager");
                        //        System.out.println(managerJson);

                        for(int ic=0; ic < managerJson.size(); ic++) {
                            JSONObject dataManager = (JSONObject) managerJson.get(ic);

                            System.out.println("IDKaryawan: " +dataManager.get("id"));
                            System.out.println("Nama: " +dataManager.get("nama"));
                            System.out.println("Tunjangan Pulsa: " +dataManager.get("tunjPulsa"));
                            System.out.println("Gaji Pokok: " +dataManager.get("gapok"));
                            System.out.println("Absensi Hari: " +dataManager.get("absensi"));
                            System.out.println("Tunjangan Transport: " +dataManager.get("tunjTransport"));
                            System.out.println("Tunjangan Entertaint: " +dataManager.get("tunjEntertaint"));

                            JSONArray teleponn = (JSONArray) dataManager.get("telepon");
                            for (Object telp: teleponn) {
                                System.out.println("Telepon: " + telp);
                            }
                        }

                    }else{
                        System.out.println("Salah pilih menu");
                    }

                    break;
            }
        }

    }
}
