package comp.Tugas1;

abstract public class Worker {
    int id;
    String nama;
    int tunjPulsa;
    int gapok;
    int absensi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getTunjPulsa() {
        return tunjPulsa;
    }

    public void setTunjPulsa(int tunjPulsa) {
        this.tunjPulsa = tunjPulsa;
    }

    public int getGapok() {
        return gapok;
    }

    public void setGapok(int gapok) {
        this.gapok = gapok;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }

}

