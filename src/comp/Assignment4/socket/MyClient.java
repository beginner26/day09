package comp.Assignment4.socket;


import comp.Assignment3.Validasi;
import comp.Assignment4.properties.CrunchifyGetPropertyValues;
import comp.ftp.FTPListDemo;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPFile;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

class tampilLayar extends Thread {
    String formated = "";

    tampilLayar(String formated) {
        this.formated = formated;
    }

    public void run() {
        System.out.println(formated);
    }
}

class tulisFile extends Thread {
    String[] parsedBySentence;

    tulisFile(String[] parsedBySentence) {
        this.parsedBySentence = parsedBySentence;
    }

    public void run() {
        String fileRata2 = "Nama,NilaiRata2\n";
        for (String dataMhs: parsedBySentence) {
            String[] parsedByWord = dataMhs.split(",");
            int total = Integer.parseInt( parsedByWord[1]) + Integer.parseInt( parsedByWord[2]) + Integer.parseInt( parsedByWord[3]);
            int rata2 = total /3;
            fileRata2 += parsedByWord[0] +","+ rata2 +"\n";

        }

        try {
            // Membuat File Output Stream
            FileOutputStream fout = new FileOutputStream("C:\\tmp\\FileRata2.txt");
            byte b[] = fileRata2.getBytes();//converting string into byte array
            fout.write(b);
            fout.close();
            System.out.println("success...");
        }catch (Exception e) {
            System.out.println(e);
        }

    }
}

class connectDanKirimFTP extends Thread {
    public void run() {
        String server = "ftp.dharmakertamandiri.co.id";
        int port = 21;
        String user = "adikbtpns@demo.dharmakertamandiri.co.id";
        String pass = "Shifted12345";

        FTPClient ftpClient = new FTPClient();
        try {

            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            // APPROACH #1: uploads first file using an InputStream
            File firstLocalFile = new File("C:/tmp/FileProses_Naufal.txt");

            String firstRemoteFile = "FileProses_Naufal.txt";
            InputStream inputStream = new FileInputStream(firstLocalFile);

            System.out.println("Start uploading first file");
            boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
            inputStream.close();
            if (done) {
                System.out.println("The first file is uploaded successfully.");
            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}


public class MyClient {


    public static void main(String[] args) {
        Validasi val = new Validasi();
        MyClient mc = new MyClient();
        Scanner input = new Scanner(System.in);

            int userPilih = 0;
            String data = "";
            CrunchifyGetPropertyValues config = new CrunchifyGetPropertyValues();
            FTPListDemo ftp = new FTPListDemo();
            int port = 0;
            String ip = "";
            String namaFile = "";

            Socket s = new Socket();
            DataOutputStream dout = null;
            DataInputStream dis = null;

            String getDataFromServer = "John,10,9,8\n" +
                    "Peter,7,8,10\n" +
                    "Steve,6,9,8";
            String formatedGetDataFromServer = "";
            String[] parseBySentence = null;

            while (userPilih != 99) {
                System.out.println("----- Menu -----");
                System.out.println("1. Connect Socket");
                System.out.println("2. Create FileProses_naufal.txt");
                System.out.println("3. Tampilkan dilayar, tulis ke file, connect ftp & kirim ftp");
                System.out.println("4. Download dari FTP Server");
                System.out.println("5. Close all conections");
                System.out.println("99. EXIT");

                System.out.print("Pilih Menu: ");
                userPilih = input.nextInt();

                switch (userPilih) {
                    case 1:
                        try {
                            port = Integer.parseInt(config.getPropValues("PORT"));
                            ip = config.getPropValues("IP");

                            s = new Socket(ip, port);

                            dis = new DataInputStream(s.getInputStream());
                            getDataFromServer = dis.readUTF();

                            System.out.println("Berhasil mendapatkan data dari server");

                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;

                    case 2:
                        try {
                            // Parsing getDataFromServer
                            parseBySentence = getDataFromServer.split("\n");
                            for (String oneSentence: parseBySentence) {
                                String[] parseByWord = oneSentence.split(",");
                                formatedGetDataFromServer += "Nama: " + parseByWord[0] + "\n" + "Nilai Fisika: " + parseByWord[1] + "\n" + "Nilai Kimia: " + parseByWord[2] + "\n" + "Nilai Biologi: " + parseByWord[3] + "\n\n";
                            }

//                            System.out.println(formatedGetDataFromServer);

                            // Membuat File Output Stream
                            FileOutputStream fout = new FileOutputStream("C:\\tmp\\FileProses_Naufal.txt");
                            byte b[] = formatedGetDataFromServer.getBytes();//converting string into byte array
                            fout.write(b);
                            fout.close();
                            System.out.println("success...");



                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;

                    case 3:
                        tampilLayar t1 = new tampilLayar(formatedGetDataFromServer);
                        t1.run();

                        // Tulis ke file
                        tulisFile t2 = new tulisFile(parseBySentence);
                        t2.run();

                        connectDanKirimFTP t3 = new connectDanKirimFTP();
                        t3.run();

                        break;

                    case 4:
                        ftp.test();
                        break;

                    case 5:
                        try {
                            dout = new DataOutputStream(s.getOutputStream());
                            dout.writeUTF("exit");
                            dout.flush();

                            System.out.println("Close all connections");

                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;
                }
            }
        try {
            s.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    }